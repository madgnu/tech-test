const SecretNotFoundError = require('./secretnotfound');
const HttpThrowableError = require('./httpthrowable');

module.exports = {
  HttpThrowableError,
  SecretNotFoundError,
};
