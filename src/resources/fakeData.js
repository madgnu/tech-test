const { faker } = require('@faker-js/faker');

module.exports = () => {
  const genData = [];
  for (let i = 0; i < 20; i += 1) {
    const rec = {
      id: i,
      name: faker.name.findName(),
      phone: Number(faker.phone.phoneNumber('###########')),
      attributes: {},
    };

    for (let j = 0; j < Math.floor(Math.random() * 5); j += 1) {
      rec.attributes[`${faker.random.word()}`] = faker.random.objectElement({
        [faker.random.word()]: faker.random.alpha(),
        [faker.random.word()]: faker.random.word(),
        [faker.random.word()]: faker.random.locale(),
        [faker.random.word()]: faker.datatype.number(),
        [faker.random.word()]: faker.datatype.boolean(),
      });
    }

    genData.push(rec);
  }
  return genData;
};
