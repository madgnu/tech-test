const express = require('express');
const db = require('./src/resources/tarantool');

const routes = require('./src/routes');
const vault = require('./src/modules/vault');
const errHandler = require('./src/middlewares/errhandler');

const app = express();

async function startup() {
  await db.initDb();
  console.log('server started');
  app.listen(vault.getSecret('PORT'));
}

app.set('view engine', 'pug');
app.set('views', './src/views');
app.use(routes);
app.use(errHandler(vault.getSecret('NODE_ENV')));

startup();
