const { con } = require('../resources/tarantool');
const msgFormat = require('../helpers/convert');

const printTable = async (req, res, next) => {
  try {
    const retData = await con.select('testspace', 'idx_id', 20, 0, 'all');
    const convertedData = retData.map((el) => msgFormat.decode(el));

    res.render('index', {
      rows: convertedData,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = {
  printTable,
};
