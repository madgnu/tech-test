# Tasks

1. Create table in Tarantool
2. Fill mockup data  (20 rows)
3. And create a simple web interface for looking at it as a table.

## env variables (.env file)

* **TARANTOOL_USER_NAME** - db user
* **TARANTOOL_USER_PASSWORD** - db password
* **TARANTOOL_HOST** - host of db (should be 'db' for docker-compose)
* **TARANTOOL_PORT** - port of db (should be 3301 for docker-compose)
* **PORT** - port to export (should be 3000 for docker-compose)

## how-to run (lazy way)

1. create .env and fill with variables
2. docker-compose build
3. docker-compose up
4. open [link](http://localhost:3000)
