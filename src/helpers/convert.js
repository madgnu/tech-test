function encode(data) {
  return [data.id, data.name, data.phone, JSON.stringify(data.attributes)];
}

function decode(data) {
  return {
    id: data[0],
    name: data[1],
    phone: data[2],
    attributes: JSON.parse(data[3]),
  };
}

module.exports = {
  encode,
  decode,
};
