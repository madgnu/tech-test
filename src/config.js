module.exports.defaults = {
  dev: {
    DB_HOST: 'localhost',
    DB_PORT: '3301',
    TARANTOOL_USER_NAME: 'user',
    TARANTOOL_PASSWORD: 'password',
  },
};
