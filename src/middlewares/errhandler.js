const { HttpThrowableError } = require('../types/errors');
const {
  ERR_MESSAGE_UNKNOWN,
} = require('../consts').messages;

const genResponseObject = (env, err) => {
  const errObj = {
    message: ERR_MESSAGE_UNKNOWN,
  };

  if (env === 'dev') {
    errObj.devData = {
      error: err.toString(),
      message: err.message,
      stack: err.stack,
    };
  }
  return errObj;
};

module.exports = (env) => (err, req, res, next) => {
  let responseCode = 500;
  const errObj = genResponseObject(env, err);

  if (err instanceof HttpThrowableError) {
    responseCode = err.statusCode;
    errObj.message = err.publicOutput;
  }
  res.status(responseCode).render('error', errObj);
  next();
};
