const router = require('express').Router();
const defaultController = require('../controllers');

router.get('/', defaultController.printTable);

module.exports = router;
