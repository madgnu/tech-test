const Tarantool = require('tarantool-driver');

const vault = require('../modules/vault');
const msgFormat = require('../helpers/convert');
const genData = require('./fakeData');

const con = new Tarantool({
  host: vault.getSecret('TARANTOOL_HOST'),
  port: vault.getSecret('TARANTOOL_PORT'),
  username: vault.getSecret('TARANTOOL_USER_NAME'),
  password: vault.getSecret('TARANTOOL_USER_PASSWORD'),
});

const initDb = async () => {
  /*
    drop space if exists
    create space
    create index
  */
  await con.eval(`
    local old_space = box.schema.space.create('testspace', {
      if_not_exists = true
    });
    old_space:drop();
    local s = box.schema.space.create('testspace', {
      if_not_exists = true
    });
    s:create_index('idx_id', {
      unique = true,
      parts = {
        1,
        'UNSIGNED'
      }
    });
  `);

  const sampleData = genData();

  // eslint-disable-next-line no-restricted-syntax
  for (const el of sampleData) {
    // eslint-disable-next-line no-await-in-loop
    await con.insert('testspace', msgFormat.encode(el));
  }

  console.log('sample data initialized');
};

module.exports = {
  initDb,
  con,
};
